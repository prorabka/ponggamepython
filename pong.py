import turtle
import os

ball_speed_modifier = -1.02
score_a = 0
score_b = 0


class Paddle(turtle.Turtle):
    """
    Paddle class. Creates instances of board to play
    """

    def __init__(self, default_position):
        super(Paddle, self).__init__()
        self.speed(0)
        self.shape('square')
        self.color('white')
        self.shapesize(stretch_len=1, stretch_wid=5)
        self.penup()
        (x, y) = default_position
        self.goto(x, y)

    def move_up(self):
        y = self.ycor()
        if y > 240:
            return

        y += 20
        self.sety(y)

    def move_down(self):
        y = self.ycor()
        if y < -230:
            return

        y -= 20
        self.sety(y)


class Ball(turtle.Turtle):
    def __init__(self):
        super(Ball, self).__init__()
        self.speed(0)
        self.shape('square')
        self.color('white')
        self.shapesize()
        self.penup()
        # set position to center
        self.goto(0, 0)
        # custom, depends on your hardware (main cycle (while) repeat rate)
        self.dx = .06
        self.dy = -.06

    def check_ycor(self):
        # check if ball reaches upside or downside
        if self.ycor() > 290 or self.ycor() < -290:
            # play hitting sound
            # for MacOS use afplay
            os.system('aplay bounce.wav&')
            # rotate the ball direction
            self.dy *= ball_speed_modifier

    def move(self):
        # move ball changing its coordinates
        self.setx(self.xcor() + self.dx)
        self.sety(self.ycor() + self.dy)

    def check_xcor(self, paddle):
        global score_a
        global score_b
        direction_rotator = 1

        if self.xcor() < 0:
            direction_rotator *= -1

        # range of coordinates of the ball  FROM ball_center_cor - 10 TO ball_center_cor + 10
        ball_size_range_cor = set(range(int(self.ycor()) - 10, int(self.ycor()) + 10))
        # range of coordinates of the player (board) FROM board_center_cor - 50 TO board_center_cor + 50
        board_size_range_cor = set(range(int(paddle.ycor()) - 50, int(paddle.ycor() + 50)))
        # if exists the intersection between board and ball
        if ball_size_range_cor & board_size_range_cor:
            # play hit sound
            os.system('aplay bounce.wav&')
            self.setx(330 * direction_rotator)
        # if no intersection -> lose
        else:
            if direction_rotator == 1:
                score_a += 1
            else:
                score_b += 1
            # move ball to center
            score.update_score()
            self.goto(0, 0)
        # change ball direction
        self.dx *= ball_speed_modifier


class Score(turtle.Turtle):
    def __init__(self):
        super(Score, self).__init__()
        self.speed(0)
        self.color('white')
        self.penup()
        self.hideturtle()
        self.goto(0, 260)
        self.write("Player A: 0 Player B: 0", align='center', font=('Courier', 25, 'normal'))

    def update_score(self):
        self.clear()
        self.write("Player A: {} Player B: {}".format(score_a, score_b), align='center', font=('Courier', 25, 'normal'))


def create_window():
    window = turtle.Screen()
    window.title('Simple Pong game for Automation')
    window.bgcolor('black')
    window.setup(width=800, height=600)
    # make window of static size
    window.cv._rootwindow.resizable(False, False)
    window.tracer(0)

    return window


if __name__ == '__main__':
    screen = create_window()

    # create 2 players (boards), ball and score label
    paddle_a = Paddle((-350, 0))
    paddle_b = Paddle((350, 0))
    ball = Ball()
    score = Score()

    screen.listen()

    screen.onkeypress(paddle_a.move_up, 'w')
    screen.onkeypress(paddle_a.move_down, 's')

    screen.onkeypress(paddle_b.move_up, 'Up')
    screen.onkeypress(paddle_b.move_down, 'Down')

    try:
        while True:
            screen.update()

            ball.check_ycor()
            ball.move()

            if 330 < ball.xcor() < 335:
                ball.check_xcor(paddle_b)
            if -335 < ball.xcor() < -330:
                ball.check_xcor(paddle_a)
    except KeyboardInterrupt:
        screen.bye()
